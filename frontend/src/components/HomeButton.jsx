import React from 'react'
import { Link } from 'react-router-dom';
import { IoIosHome } from "react-icons/io";


const HomeButton = ({ destination = '/' }) => {
    return (
        <div className='flex'>
            <Link
                to={destination}
                className='bg-sky-800 text-white px-4 py-1 rounded-1g w-fit'>
                <IoIosHome className='text-2xl' />
            </Link>
        </div>
    )
}

export default HomeButton