import React from 'react';

const Navbar = ({ pageTitle, onHomeClick, onCreateClick }) => {
    return (
        <nav>
            <div>
                <button className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded' onClick={onHomeClick}>Home</button>
                <h1 className="text-white text-2xl font-bold">{pageTitle}</h1>
                <button className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded' onClick={onCreateClick}>Create</button>
            </div>
        </nav>
    );
};

export default Navbar;