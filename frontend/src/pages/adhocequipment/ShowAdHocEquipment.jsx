import React, { useEffect, useState } from 'react';
import axios from 'axios'
import { useParams } from 'react-router-dom'
import HomeButton from '../../components/HomeButton';
import Spinner from '../../components/Spinner';

const ShowAdHocEquipment = () => {
    const [adHocEquipment, setAdHocEquipment] = useState({});
    const [loading, setLoading] = useState(false);
    const { id } = useParams();

    useEffect(() => {
        setLoading(true);
        axios
            .get(`http://localhost:5555/adHocEquipment/${id}`)
            .then((response) => {
                setAdHocEquipment(response.data);
                setLoading(false);
            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            })
    }, [])

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Show AdHocEquipment</h1>
            {loading ? (
                <Spinner />
            ) : (
                <div className='flex flex-col border-2 border-sky-400 rounded-xl w-fit p-4'>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>ID</span>
                        <span>{adHocEquipment._id}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Name</span>
                        <span>{adHocEquipment.name}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Serial No</span>
                        <span>{adHocEquipment.serialNo}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Staff Contact</span>
                        <span>{adHocEquipment.staffContact}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Creation Time</span>
                        <span>{new Date(adHocEquipment.createdAt).toString()}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Last Update Time</span>
                        <span>{new Date(adHocEquipment.updatedAt).toString()}</span>
                    </div>
                </div>
            )}
        </div>
    )
}

export default ShowAdHocEquipment