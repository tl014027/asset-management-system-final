import React, { useState } from 'react'
import HomeButton from '../../components/HomeButton'
import Spinner from '../../components/Spinner'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const CreateAnnualSoftwareRequest = () => {
    const [name, setName] = useState('');
    const [version, setVersion] = useState('');
    const [description, setDescription] = useState('');
    const [configuration, setConfiguration] = useState('');
    const [operatingSystem, setOperatingSystem] = useState('');
    const [additionalHardware, setAdditionalHardware] = useState('');
    const [contact, setContact] = useState('');
    const [testSteps, setTestSteps] = useState('');
    const [dateRequired, setDateRequired] = useState('');
    const [offCampus, setOffCampus] = useState('');
    const [whichLocations, setWhichLocation] = useState('');
    const [accessLimit, setAccessLimit] = useState('');
    const [licenceHeld, setLicenceHeld] = useState('');
    const [productManufacturer, setProductManufacturer] = useState('');
    const [other, setOther] = useState('');
    const [changeTicket, setChangeTicket] = useState('');
    const [dateRequested, setDateRequested] = useState('');
    const [status, setStatus] = useState('');
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate();

    // TODO: Check for publish year is not in future
    const handleSaveAnnualSoftwareRequest = () => {
        const data = {
            name,
            version,
            description,
            configuration,
            operatingSystem,
            additionalHardware,
            contact,
            testSteps,
            dateRequired,
            offCampus,
            whichLocations,
            accessLimit,
            licenceHeld,
            productManufacturer,
            other,
            changeTicket,
            dateRequested,
            status
        };
        setLoading(true);
        axios
            .post('http://localhost:5555/annualSoftwareRequest', data)
            .then(() => {
                setLoading(false);
                navigate('/annualSoftwareRequest')
            })
            .catch((error) => {
                setLoading(false);
                alert('Error: Please check console');
                console.log(error);
            })
    }

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Create Annual Software Request</h1>
            {loading ? <Spinner /> : ''}
            <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-4 mx-auto'>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Name</label>
                    <input
                        type='text'
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Version</label>
                    <input
                        type='text'
                        value={version}
                        onChange={(e) => setVersion(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Description</label>
                    <input
                        type='text'
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Configuration</label>
                    <input
                        type='text'
                        value={configuration}
                        onChange={(e) => setConfiguration(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Operating System</label>
                    <input
                        type='text'
                        value={operatingSystem}
                        onChange={(e) => setOperatingSystem(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Additional Hardware</label>
                    <input
                        type='text'
                        value={additionalHardware}
                        onChange={(e) => setAdditionalHardware(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Contact</label>
                    <input
                        type='text'
                        value={contact}
                        onChange={(e) => setContact(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Test Steps</label>
                    <input
                        type='text'
                        value={testSteps}
                        onChange={(e) => setTestSteps(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Date Required</label>
                    <input
                        type='text'
                        value={dateRequired}
                        onChange={(e) => setDateRequired(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Off Campus</label>
                    <input
                        type='text'
                        value={offCampus}
                        onChange={(e) => setOffCampus(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Which Locations</label>
                    <input
                        type='text'
                        value={whichLocations}
                        onChange={(e) => setWhichLocation(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Access Limit</label>
                    <input
                        type='text'
                        value={accessLimit}
                        onChange={(e) => setAccessLimit(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Licence Held</label>
                    <input
                        type='text'
                        value={licenceHeld}
                        onChange={(e) => setLicenceHeld(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Product Manufacturer</label>
                    <input
                        type='text'
                        value={productManufacturer}
                        onChange={(e) => setProductManufacturer(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Other</label>
                    <input
                        type='text'
                        value={other}
                        onChange={(e) => setOther(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Change Ticket</label>
                    <input
                        type='text'
                        value={changeTicket}
                        onChange={(e) => setChangeTicket(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Date Requested</label>
                    <input
                        type='text'
                        value={dateRequested}
                        onChange={(e) => setDateRequested(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Status</label>
                    <input
                        type='text'
                        value={status}
                        onChange={(e) => setStatus(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <button className='p-2 bg-sky-300 m-8' onClick={handleSaveAnnualSoftwareRequest}>
                    Save
                </button>
            </div>
        </div>
    )
}

export default CreateAnnualSoftwareRequest