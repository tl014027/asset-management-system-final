import React, { useEffect, useState } from 'react';
import axios from 'axios'
import { useParams } from 'react-router-dom'
import HomeButton from '../../components/HomeButton';
import Spinner from '../../components/Spinner';

const ShowAnnualSoftwareRequest = () => {
    const [annualSoftwareRequest, setAnnualSoftwareRequest] = useState({});
    const [loading, setLoading] = useState(false);
    const { id } = useParams();

    useEffect(() => {
        setLoading(true);
        axios
            .get(`http://localhost:5555/annualSoftwareRequest/${id}`)
            .then((response) => {
                setAnnualSoftwareRequest(response.data);
                setLoading(false);
            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            })
    }, [])

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Show Annual Software Request</h1>
            {loading ? (
                <Spinner />
            ) : (
                <div className='flex flex-col border-2 border-sky-400 rounded-xl w-fit p-4'>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>ID</span>
                        <span>{annualSoftwareRequest._id}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Name</span>
                        <span>{annualSoftwareRequest.name}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Version</span>
                        <span>{annualSoftwareRequest.version}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Description</span>
                        <span>{annualSoftwareRequest.description}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Configuration</span>
                        <span>{annualSoftwareRequest.configuration}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Operating System</span>
                        <span>{annualSoftwareRequest.operatingSystem}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Additional Hardware</span>
                        <span>{annualSoftwareRequest.additionalHardware}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Contact</span>
                        <span>{annualSoftwareRequest.contact}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Test Steps</span>
                        <span>{annualSoftwareRequest.testSteps}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Date Required</span>
                        <span>{annualSoftwareRequest.dateRequired}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Off Campus</span>
                        <span>{annualSoftwareRequest.offCampus}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Which Locations</span>
                        <span>{annualSoftwareRequest.whichLocations}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Access Limit</span>
                        <span>{annualSoftwareRequest.accessLimit}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Licence Held</span>
                        <span>{annualSoftwareRequest.licenceHeld}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Product Manufacturer</span>
                        <span>{annualSoftwareRequest.productManufacturer}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Other</span>
                        <span>{annualSoftwareRequest.other}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Change Ticket</span>
                        <span>{annualSoftwareRequest.changeTicket}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Date Requested</span>
                        <span>{annualSoftwareRequest.dateRequested}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Status</span>
                        <span>{annualSoftwareRequest.status}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Creation Time</span>
                        <span>{new Date(annualSoftwareRequest.createdAt).toString()}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Last Update Time</span>
                        <span>{new Date(annualSoftwareRequest.updatedAt).toString()}</span>
                    </div>
                </div>
            )}
        </div>
    )
}

export default ShowAnnualSoftwareRequest