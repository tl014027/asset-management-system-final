import React from 'react';
import { Link } from 'react-router-dom';
import Navbar from '../components/Navbar';

const Homepage = () => {
    return (
        <div className='p-4 flex flex-col items-center justify-center h-screen'>
            <h1 className='text-4xl mb-8'>Reading University Asset Tracker</h1>
            <Link to="/computer" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>Computers</Link>
            <Link to="/homeequipment" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>Home Equipment</Link>
            <Link to="/adhocequipment" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>Ad Hoc Equipment</Link>
            <Link to="/ageofstaffhardware" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>Age Of Staff Hardware</Link>
            <Link to="/annualsoftwarerequest" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>Annual Software Request</Link>
            <Link to="/peopleneedingnewhardware" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>People Needing New Hardware</Link>
            <Link to="/phdlaptopallocation" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>PHD Laptop Allocation</Link>
            <Link to="/softwarelicences" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>Software Licences</Link>
            <Link to="/softwaremodulemapping" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>Software Module Mapping</Link>
            <Link to="/unallocatedequipment" className='p-4 bg-blue-500 text-white m-4 w-64 text-center rounded'>Unallocated Equipment</Link>
        </div>
    );
};

export default Homepage;