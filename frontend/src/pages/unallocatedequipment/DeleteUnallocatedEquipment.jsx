import React, { useState } from 'react'
import HomeButton from '../../components/HomeButton'
import Spinner from '../../components/Spinner'
import axios from 'axios'
import { useNavigate, useParams } from 'react-router-dom'

const DeleteUnallocatedEquipment = () => {
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();
    const { id } = useParams();
    const handleDeleteUnallocatedEquipment = () => {

        setLoading(true);
        axios
            .delete(`http://localhost:5555/unallocatedEquipment/${id}`)
            .then(() => {
                setLoading(false);
                navigate('/unallocatedEquipment');
            })
            .catch((error) => {
                setLoading(false);
                alert('Error: please check console')
                console.log(error);
            })
    };

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl, my-4'>Delete UnallocatedEquipment</h1>
            {loading ? <Spinner /> : ''}
            <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-8 mx-auto'>
                <h3 className='text-2xl mx-auto'>Are you sure you want to delete this item?</h3>
                <button className='p-4 bg-red-600 rounded-xl text-white m-8 w-full mx-auto' onClick={handleDeleteUnallocatedEquipment}>Yes</button>
            </div>
        </div>
    )
}

export default DeleteUnallocatedEquipment