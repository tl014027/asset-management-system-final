import React, { useState, useEffect } from 'react'
import HomeButton from '../../components/HomeButton'
import Spinner from '../../components/Spinner'
import axios from 'axios'
import { useNavigate, useParams } from 'react-router-dom'

const EditUnallocatedEquipment = () => {
    const [program, setProgram] = useState('');
    const [licenceNo, setLicenceNo] = useState('');
    const [expiry, setExpiry] = useState('');
    const [numberOfLicences, setNumberOfLicences] = useState('');
    const [memberOfStaff, setMemberOfStaff] = useState('');
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate();
    const { id } = useParams();
    useEffect(() => {
        setLoading(true);
        axios
            .get(`http://localhost:5555/unallocatedEquipment/${id}`)
            .then((response) => {
                setProgram(response.data.program);
                setLicenceNo(response.data.licenceNo);
                setExpiry(response.data.expiry);
                setNumberOfLicences(response.data.numberOfLicences);
                setMemberOfStaff(response.data.memberOfStaff);
                setLoading(false);
            }).catch((error) => {
                setLoading(false);
                alert('Error: Please check the console');
                console.log(error);
            })
    }, [id]);
    const handleEditUnallocatedEquipment = () => {
        const data = {
            program: program,
            licenceNo: licenceNo,
            expiry: expiry,
            numberOfLicences: numberOfLicences,
            memberOfStaff: memberOfStaff
        };
        setLoading(true);
        axios
            .put(`http://localhost:5555/unallocatedEquipment/${id}`, data)
            .then(() => {
                setLoading(false);
                navigate('/unallocatedEquipment')
            })
            .catch((error) => {
                setLoading(false);
                alert('Error: Please check console');
                console.log(error);
            })
    }

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Edit Ad Hoc Equipment</h1>
            {loading ? <Spinner /> : ''}
            <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-4 mx-auto'>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Program</label>
                    <input
                        type='text'
                        value={program}
                        onChange={(e) => setProgram(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Licence Number</label>
                    <input
                        type='text'
                        value={licenceNo}
                        onChange={(e) => setLicenceNo(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Expiry</label>
                    <input
                        type='text'
                        value={expiry}
                        onChange={(e) => setExpiry(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Number Of Licences</label>
                    <input
                        type='text'
                        value={numberOfLicences}
                        onChange={(e) => setNumberOfLicences(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Member Of Staff</label>
                    <input
                        type='text'
                        value={memberOfStaff}
                        onChange={(e) => setMemberOfStaff(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <button className='p-2 bg-sky-300 m-8' onClick={handleEditUnallocatedEquipment}>
                    Save
                </button>
            </div>
        </div>
    )
}

export default EditUnallocatedEquipment