import React, { useEffect, useState } from 'react';
import axios from 'axios'
import { useParams } from 'react-router-dom'
import HomeButton from '../../components/HomeButton';
import Spinner from '../../components/Spinner';

const ShowPhdLaptopAllocation = () => {
    const [phdLaptopAllocation, setPhdLaptopAllocation] = useState({});
    const [loading, setLoading] = useState(false);
    const { id } = useParams();

    useEffect(() => {
        setLoading(true);
        axios
            .get(`http://localhost:5555/phdLaptopAllocation/${id}`)
            .then((response) => {
                setPhdLaptopAllocation(response.data);
                setLoading(false);
            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            })
    }, [])

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Show PhdLaptopAllocation</h1>
            {loading ? (
                <Spinner />
            ) : (
                <div className='flex flex-col border-2 border-sky-400 rounded-xl w-fit p-4'>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>ID</span>
                        <span>{phdLaptopAllocation._id}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Model</span>
                        <span>{phdLaptopAllocation.model}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Student</span>
                        <span>{phdLaptopAllocation.student}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Supervisor</span>
                        <span>{phdLaptopAllocation.supervisor}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>MAC Address</span>
                        <span>{phdLaptopAllocation.macAddress}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Serial Number</span>
                        <span>{phdLaptopAllocation.serialNo}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Purchase Order</span>
                        <span>{phdLaptopAllocation.purchaseOrder}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Month Allocated</span>
                        <span>{phdLaptopAllocation.monthAllocated}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Month Returned</span>
                        <span>{phdLaptopAllocation.monthReturned}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Creation Time</span>
                        <span>{new Date(phdLaptopAllocation.createdAt).toString()}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Last Update Time</span>
                        <span>{new Date(phdLaptopAllocation.updatedAt).toString()}</span>
                    </div>
                </div>
            )}
        </div>
    )
}

export default ShowPhdLaptopAllocation