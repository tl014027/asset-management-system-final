import React, { useState, useEffect } from 'react'
import HomeButton from '../../components/HomeButton'
import Spinner from '../../components/Spinner'
import axios from 'axios'
import { useNavigate, useParams } from 'react-router-dom'

const EditPhdLaptopAllocation = () => {
    const [model, setModel] = useState('');
    const [student, setStudent] = useState('');
    const [supervisor, setSupervisor] = useState('');
    const [macAddress, setMacAddress] = useState('');
    const [serialNo, setSerialNo] = useState('');
    const [purchaseOrder, setPurchaseOrder] = useState('');
    const [monthAllocated, setMonthAllocated] = useState('');
    const [monthReturned, setMonthReturned] = useState('');
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate();
    const { id } = useParams();
    useEffect(() => {
        setLoading(true);
        axios
            .get(`http://localhost:5555/phdLaptopAllocation/${id}`)
            .then((response) => {
                setName(response.data.name);
                setSerialNo(response.data.serialNo);
                setStaffContact(response.data.staffContact);
                setLoading(false);
            }).catch((error) => {
                setLoading(false);
                alert('Error: Please check the console');
                console.log(error);
            })
    }, [id]);
    const handleEditPhdLaptopAllocation = () => {
        const data = {
            name: name,
            serialNo: serialNo,
            staffContact: staffContact,
        };
        setLoading(true);
        axios
            .put(`http://localhost:5555/phdLaptopAllocation/${id}`, data)
            .then(() => {
                setLoading(false);
                navigate('/phdLaptopAllocation')
            })
            .catch((error) => {
                setLoading(false);
                alert('Error: Please check console');
                console.log(error);
            })
    }

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Edit Ad Hoc Equipment</h1>
            {loading ? <Spinner /> : ''}
            <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-4 mx-auto'>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Model</label>
                    <input
                        type='text'
                        value={model}
                        onChange={(e) => setModel(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Student</label>
                    <input
                        type='text'
                        value={student}
                        onChange={(e) => setStudent(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Supervisor</label>
                    <input
                        type='text'
                        value={supervisor}
                        onChange={(e) => setSupervisor(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>MAC Address</label>
                    <input
                        type='text'
                        value={macAddress}
                        onChange={(e) => setMacAddress(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Serial Number</label>
                    <input
                        type='text'
                        value={serialNo}
                        onChange={(e) => setSerialNo(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Purchase Order</label>
                    <input
                        type='text'
                        value={purchaseOrder}
                        onChange={(e) => setPurchaseOrder(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Month Allocated</label>
                    <input
                        type='text'
                        value={monthAllocated}
                        onChange={(e) => setMonthAllocated(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Month Returned</label>
                    <input
                        type='text'
                        value={monthReturned}
                        onChange={(e) => setMonthReturned(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <button className='p-2 bg-sky-300 m-8' onClick={handleEditPhdLaptopAllocation}>
                    Save
                </button>
            </div>
        </div>
    )
}

export default EditPhdLaptopAllocation