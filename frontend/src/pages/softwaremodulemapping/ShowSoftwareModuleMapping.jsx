import React, { useEffect, useState } from 'react';
import axios from 'axios'
import { useParams } from 'react-router-dom'
import HomeButton from '../../components/HomeButton';
import Spinner from '../../components/Spinner';

const ShowSoftwareModuleMapping = () => {
    const [softwareModuleMapping, setSoftwareModuleMapping] = useState({});
    const [loading, setLoading] = useState(false);
    const { id } = useParams();

    useEffect(() => {
        setLoading(true);
        axios
            .get(`http://localhost:5555/softwareModuleMapping/${id}`)
            .then((response) => {
                setSoftwareModuleMapping(response.data);
                setLoading(false);
            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            })
    }, [])

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Show SoftwareModuleMapping</h1>
            {loading ? (
                <Spinner />
            ) : (
                <div className='flex flex-col border-2 border-sky-400 rounded-xl w-fit p-4'>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>ID</span>
                        <span>{softwareModuleMapping._id}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Name</span>
                        <span>{softwareModuleMapping.name}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Serial No</span>
                        <span>{softwareModuleMapping.serialNo}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Staff Contact</span>
                        <span>{softwareModuleMapping.staffContact}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Creation Time</span>
                        <span>{new Date(softwareModuleMapping.createdAt).toString()}</span>
                    </div>
                    <div className='my-4'>
                        <span className='text-xl mr-4 text-gray-500'>Last Update Time</span>
                        <span>{new Date(softwareModuleMapping.updatedAt).toString()}</span>
                    </div>
                </div>
            )}
        </div>
    )
}

export default ShowSoftwareModuleMapping