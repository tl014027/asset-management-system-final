import React, { useState } from 'react'
import HomeButton from '../../components/HomeButton'
import Spinner from '../../components/Spinner'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const CreateSoftwareModuleMapping = () => {
    const [name, setName] = useState('');
    const [serialNo, setSerialNo] = useState('');
    const [staffContact, setStaffContact] = useState('');
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate();

    // TODO: Check for publish year is not in future
    const handleSaveSoftwareModuleMapping = () => {
        const data = {
            name,
            serialNo,
            staffContact
        };
        setLoading(true);
        axios
            .post('http://localhost:5555/softwareModuleMapping', data)
            .then(() => {
                setLoading(false);
                navigate('/softwareModuleMapping')
            })
            .catch((error) => {
                setLoading(false);
                alert('Error: Please check console');
                console.log(error);
            })
    }

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Create SoftwareModuleMapping</h1>
            {loading ? <Spinner /> : ''}
            <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-4 mx-auto'>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Name</label>
                    <input
                        type='text'
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Serial No</label>
                    <input
                        type='text'
                        value={serialNo}
                        onChange={(e) => setSerialNo(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Staff Contact</label>
                    <input
                        type='text'
                        value={staffContact}
                        onChange={(e) => setStaffContact(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <button className='p-2 bg-sky-300 m-8' onClick={handleSaveSoftwareModuleMapping}>
                    Save
                </button>
            </div>
        </div>
    )
}

export default CreateSoftwareModuleMapping