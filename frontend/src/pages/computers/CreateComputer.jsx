import React, { useState } from 'react'
import HomeButton from '../../components/HomeButton'
import Spinner from '../../components/Spinner'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const CreateComputer = () => {
    const [type, setType] = useState('');
    const [model, setModel] = useState('');
    const [characteristics, setCharacteristics] = useState('');
    const [responsibleStaff, setResponsibleStaff] = useState('');
    const [location, setLocation] = useState('');
    const [machineStatus, setMachineStatus] = useState('');
    const [macAddress, setMacAddress] = useState('');
    const [assetTag, setAssetTag] = useState('');
    const [invNo, setInvNo] = useState('');
    const [graphicsCard, setGraphicsCard] = useState('');
    const [comments, setcomments] = useState('');
    const [serialNo, setserialNo] = useState('');
    const [alternateUsername, setalternateUsername] = useState('');
    const [warrantyStartDate, setwarrantyStartDate] = useState('');
    const [orderNo, setorderNo] = useState('');
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate();

    // TODO: Check for publish year is not in future
    const handleSaveComputer = () => {
        const data = {
            type,
            model,
            characteristics,
            responsibleStaff,
            location,
            machineStatus,
            macAddress,
            assetTag,
            invNo,
            graphicsCard,
            comments,
            serialNo,
            alternateUsername,
            warrantyStartDate,
            orderNo
        };
        setLoading(true);
        axios
            .post('http://localhost:5555/computer', data)
            .then(() => {
                setLoading(false);
                navigate('/computer')
            })
            .catch((error) => {
                setLoading(false);
                alert('Error: Please check console');
                console.log(error);
            })
    }

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Create Computer</h1>
            {loading ? <Spinner /> : ''}
            <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-4 mx-auto'>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Type</label>
                    <input
                        type='text'
                        value={type}
                        onChange={(e) => setType(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Model</label>
                    <input
                        type='text'
                        value={model}
                        onChange={(e) => setModel(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Characteristics</label>
                    <input
                        type='text'
                        value={characteristics}
                        onChange={(e) => setCharacteristics(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Responsible Staff</label>
                    <input
                        type='text'
                        value={responsibleStaff}
                        onChange={(e) => setResponsibleStaff(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Location</label>
                    <input
                        type='text'
                        value={location}
                        onChange={(e) => setLocation(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Machine Status</label>
                    <input
                        type='text'
                        value={machineStatus}
                        onChange={(e) => setMachineStatus(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>MAC Address</label>
                    <input
                        type='text'
                        value={macAddress}
                        onChange={(e) => setMacAddress(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Asset Tag</label>
                    <input
                        type='text'
                        value={assetTag}
                        onChange={(e) => setAssetTag(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Invoice No</label>
                    <input
                        type='text'
                        value={invNo}
                        onChange={(e) => setInvNo(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Graphics Card</label>
                    <input
                        type='text'
                        value={graphicsCard}
                        onChange={(e) => setGraphicsCard(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Comments</label>
                    <input
                        type='text'
                        value={comments}
                        onChange={(e) => setcomments(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Serial No</label>
                    <input
                        type='text'
                        value={serialNo}
                        onChange={(e) => setserialNo(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Alternate Username</label>
                    <input
                        type='text'
                        value={alternateUsername}
                        onChange={(e) => setalternateUsername(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Warranty Start Date</label>
                    <input
                        type='text'
                        value={warrantyStartDate}
                        onChange={(e) => setwarrantyStartDate(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Order No</label>
                    <input
                        type='text'
                        value={orderNo}
                        onChange={(e) => setorderNo(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <button className='p-2 bg-sky-300 m-8' onClick={handleSaveComputer}>
                    Save
                </button>
            </div>
        </div>
    )
}

export default CreateComputer