import React, { useState } from 'react'
import HomeButton from '../../components/HomeButton'
import Spinner from '../../components/Spinner'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const CreateAgeOfStaffHardware = () => {
    const [hardwareType, setHardwareType] = useState('');
    const [hardwareModel, setHardwareModel] = useState('');
    const [hardwareProcessor, setHardwareProcessor] = useState('');
    const [hardwareLocation, setHardwareLocation] = useState('');
    const [inuse, setInUse] = useState('');
    const [dispose, setDispose] = useState('');
    const [age, setAge] = useState('');
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate();

    // TODO: Check for publish year is not in future
    const handleSaveAgeOfStaffHardware = () => {
        const data = {
            hardwareType: hardwareType,
            hardwareModel: hardwareModel,
            hardwareProcessor: hardwareProcessor,
            hardwareLocation: hardwareLocation,
            inuse: inuse,
            dispose: dispose,
            age: age
        };
        setLoading(true);
        axios
            .post('http://localhost:5555/ageOfStaffHardware', data)
            .then(() => {
                setLoading(false);
                navigate('/ageOfStaffHardware')
            })
            .catch((error) => {
                setLoading(false);
                alert('Error: Please check console');
                console.log(error);
            })
    }

    return (
        <div className='p-4'>
            <HomeButton />
            <h1 className='text-3xl my-4'>Create Age Of Staff Hardware</h1>
            {loading ? <Spinner /> : ''}
            <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-4 mx-auto'>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Type</label>
                    <input
                        type='text'
                        value={hardwareType}
                        onChange={(e) => setHardwareType(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Model</label>
                    <input
                        type='text'
                        value={hardwareModel}
                        onChange={(e) => setHardwareModel(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Processor</label>
                    <input
                        type='text'
                        value={hardwareProcessor}
                        onChange={(e) => setHardwareProcessor(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Location</label>
                    <input
                        type='text'
                        value={hardwareLocation}
                        onChange={(e) => setHardwareLocation(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>In Use</label>
                    <input
                        type='text'
                        value={inuse}
                        onChange={(e) => setInUse(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Dispose</label>
                    <input
                        type='text'
                        value={dispose}
                        onChange={(e) => setDispose(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <div className='my-4'>
                    <label className='text-xl mr-4 text-gray-500'>Age</label>
                    <input
                        type='text'
                        value={age}
                        onChange={(e) => setAge(e.target.value)}
                        className='border-2 border-gray-500 px-4 py-2 w-full'
                    />
                </div>
                <button className='p-2 bg-sky-300 m-8' onClick={handleSaveAgeOfStaffHardware}>
                    Save
                </button>
            </div>
        </div>
    )
}

export default CreateAgeOfStaffHardware