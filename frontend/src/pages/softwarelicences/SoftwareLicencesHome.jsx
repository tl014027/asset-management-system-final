import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Spinner from '../../components/Spinner'
import { Link } from 'react-router-dom'
import { AiOutlineEdit } from 'react-icons/ai'
import { BsInfoCircle } from 'react-icons/bs'
import { MdOutlineAddBox, MdOutlineDelete } from 'react-icons/md'
import HomeButton from '../../components/HomeButton'

const SoftwareLicencesHome = () => {

    const [softwareLicences, setSoftwareLicences] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        axios
            .get('http://localhost:5555/softwareLicences')
            .then((response) => {
                setSoftwareLicences(response.data.data);
                setLoading(false);
            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            })
    }, []);

    return (
        <div className='p-4'>
            <div className='flex justify-between items-center'>
                <h1 className='text-3xl my-8'>Software Licences List</h1>
                <Link to='/softwareLicences/create'>
                    <MdOutlineAddBox className='text-sky-800 text-4xl'></MdOutlineAddBox>
                </Link>
                <HomeButton />
            </div>
            {loading ? (
                <Spinner />
            ) : (
                <table className='w-full border-separate border-spacing-2'>
                    <thead>
                        <tr>
                            <th className='border border-slate-600 rounded-md'>Index</th>
                            <th className='border border-slate-600 rounded-md'>Program</th>
                            <th className='border border-slate-600 rounded-md max-md:hidden'>Expiry</th>
                            <th className='border border-slate-600 rounded-md max-md:hidden'>Number Of Licences</th>
                            <th className='border border-slate-600 rounded-md'>Operations</th>
                        </tr>
                    </thead>
                    <tbody>
                        {softwareLicences.map((softwareLicences, index) => {
                            return (
                                <tr key={softwareLicences._id} className='h-8'>
                                    <td className='border border-slate-700 rounded-md text-center'>
                                        {index + 1}
                                    </td>
                                    <td className='border border-slate-700 rounded-md text-center'>
                                        {softwareLicences.program}
                                    </td>
                                    <td className='border border-slate-700 rounded-md text-center max-md:hidden'>
                                        {softwareLicences.expiry}
                                    </td>
                                    <td className='border border-slate-700 rounded-md text-center max-md:hidden'>
                                        {softwareLicences.numberOfLicences}
                                    </td>
                                    <td className='border border-slate-700 rounded-md text-center'>
                                        <div className='flex justify-center gap-x-4'>
                                            <Link to={`/softwareLicences/details/${softwareLicences._id}`}>
                                                <BsInfoCircle className='text-2xl text-green-800' />
                                            </Link>
                                            <Link to={`/softwareLicences/edit/${softwareLicences._id}`}>
                                                <AiOutlineEdit className='text-2xl text-yellow-600' />
                                            </Link>
                                            <Link to={`/softwareLicences/delete/${softwareLicences._id}`}>
                                                <MdOutlineDelete className='text-2xl text-red-600' />
                                            </Link>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            )}
        </div>
    )
}

export default SoftwareLicencesHome