import React from 'react'
import { Routes, Route } from 'react-router-dom'

import Homepage from './pages/Homepage';

import ComputersHome from './pages/computers/ComputersHome';
import CreateComputer from './pages/computers/CreateComputer';
import DeleteComputer from './pages/computers/DeleteComputer';
import ShowComputer from './pages/computers/ShowComputer';
import EditComputer from './pages/computers/EditComputer';

import HomeEquipmentHome from './pages/homeEquipment/HomeEquipmentHome';
import CreateHomeEquipment from './pages/homeEquipment/CreateHomeEquipment';
import ShowHomeEquipment from './pages/homeEquipment/ShowHomeEquipment';
import DeleteHomeEquipment from './pages/homeEquipment/DeleteHomeEquipment';
import EditHomeEquipment from './pages/homeEquipment/EditHomeEquipment';

import AdHocEquipmentsHome from './pages/adhocequipment/AdHocEquipmentHome';
import CreateAdHocEquipment from './pages/adhocequipment/CreateAdHocEquipment';
import ShowAdHocEquipment from './pages/adhocequipment/ShowAdHocEquipment';
import DeleteAdHocEquipment from './pages/adhocequipment/DeleteAdHocEquipment';
import EditAdHocEquipment from './pages/adhocequipment/EditAdHocEquipment';

import AgeOfStaffHardwareHome from './pages/ageofstaffhardware/AgeOfStaffHardwareHome';
import CreateAgeOfStaffHardware from './pages/ageofstaffhardware/CreateAgeOfStaffHardware';
import ShowAgeOfStaffHardware from './pages/ageofstaffhardware/ShowAgeOfStaffHardware';
import DeleteAgeOfStaffHardware from './pages/ageofstaffhardware/DeleteAgeOfStaffHardware';
import EditAgeOfStaffHardware from './pages/ageofstaffhardware/EditAgeOfStaffHardware';

import AnnualSoftwareRequestHome from './pages/annualsoftwarerequest/AnnualSoftwareRequestHome';
import CreateAnnualSoftwareRequest from './pages/annualsoftwarerequest/CreateAnnualSoftwareRequest';
import ShowAnnualSoftwareRequest from './pages/annualsoftwarerequest/ShowAnnualSoftwareRequest';
import DeleteAnnualSoftwareRequest from './pages/annualsoftwarerequest/DeleteAnnualSoftwareRequest';
import EditAnnualSoftwareRequest from './pages/annualsoftwarerequest/EditAnnualSoftwareRequest';

import PeopleNeedingNewHardwareHome from './pages/peopleneedingnewhardware/PeopleNeedingNewHardwareHome';
import CreatePeopleNeedingNewHardware from './pages/peopleneedingnewhardware/CreatePeopleNeedingNewHardware';
import ShowPeopleNeedingNewHardware from './pages/peopleneedingnewhardware/ShowPeopleNeedingNewHardware';
import DeletePeopleNeedingNewHardware from './pages/peopleneedingnewhardware/DeletePeopleNeedingNewHardware';
import EditPeopleNeedingNewHardware from './pages/peopleneedingnewhardware/EditPeopleNeedingNewHardware';

import PhdLaptopAllocationsHome from './pages/phdlaptopallocation/PhdLaptopAllocationHome';
import CreatePhdLaptopAllocation from './pages/phdlaptopallocation/CreatePhdLaptopAllocation';
import ShowPhdLaptopAllocation from './pages/phdlaptopallocation/ShowPhdLaptopAllocation';
import DeletePhdLaptopAllocation from './pages/phdlaptopallocation/DeletePhdLaptopAllocation';
import EditPhdLaptopAllocation from './pages/phdlaptopallocation/EditPhdLaptopAllocation';

import SoftwareLicencesHome from './pages/softwarelicences/SoftwareLicencesHome';
import CreateSoftwareLicences from './pages/softwarelicences/CreateSoftwareLicences';
import ShowSoftwareLicences from './pages/softwarelicences/ShowSoftwareLicences';
import DeleteSoftwareLicences from './pages/softwarelicences/DeleteSoftwareLicences';
import EditSoftwareLicences from './pages/softwarelicences/EditSoftwareLicences';

import SoftwareModuleMappingHome from './pages/softwaremodulemapping/SoftwareModuleMappingHome';
import CreateSoftwareModuleMapping from './pages/softwaremodulemapping/CreateSoftwareModuleMapping';
import ShowSoftwareModuleMapping from './pages/softwaremodulemapping/ShowSoftwareModuleMapping';
import DeleteSoftwareModuleMapping from './pages/softwaremodulemapping/DeleteSoftwareModuleMapping';
import EditSoftwareModuleMapping from './pages/softwaremodulemapping/EditSoftwareModuleMapping';

import UnallocatedEquipmentHome from './pages/unallocatedequipment/UnallocatedEquipmentHome';
import CreateUnallocatedEquipment from './pages/unallocatedequipment/CreateUnallocatedEquipment';
import ShowUnallocatedEquipment from './pages/unallocatedequipment/ShowUnallocatedEquipment';
import DeleteUnallocatedEquipment from './pages/unallocatedequipment/DeleteUnallocatedEquipment';
import EditUnallocatedEquipment from './pages/unallocatedequipment/EditUnallocatedEquipment';

const App = () => {
  return (
    <Routes>
      <Route path='/' element={<Homepage />} />

      <Route path='/computer' element={<ComputersHome />} />
      <Route path='/computer/create' element={<CreateComputer />} />
      <Route path='/computer/details/:id' element={<ShowComputer />} />
      <Route path='/computer/edit/:id' element={<EditComputer />} />
      <Route path='/computer/delete/:id' element={<DeleteComputer />} />

      <Route path='/homeequipment' element={<HomeEquipmentHome />} />
      <Route path='/homeequipment/create' element={<CreateHomeEquipment />} />
      <Route path='/homeequipment/details/:id' element={<ShowHomeEquipment />} />
      <Route path='/homeequipment/edit/:id' element={<EditHomeEquipment />} />
      <Route path='/homeequipment/delete/:id' element={<DeleteHomeEquipment />} />

      <Route path='/adhocequipment' element={<AdHocEquipmentsHome />} />
      <Route path='/adhocequipment/create' element={<CreateAdHocEquipment />} />
      <Route path='/adhocequipment/details/:id' element={<ShowAdHocEquipment />} />
      <Route path='/adhocequipment/edit/:id' element={<EditAdHocEquipment />} />
      <Route path='/adhocequipment/delete/:id' element={<DeleteAdHocEquipment />} />

      <Route path='/ageofstaffhardware' element={<AgeOfStaffHardwareHome />} />
      <Route path='/ageofstaffhardware/create' element={<CreateAgeOfStaffHardware />} />
      <Route path='/ageofstaffhardware/details/:id' element={<ShowAgeOfStaffHardware />} />
      <Route path='/ageofstaffhardware/edit/:id' element={<EditAgeOfStaffHardware />} />
      <Route path='/ageofstaffhardware/delete/:id' element={<DeleteAgeOfStaffHardware />} />

      <Route path='/annualsoftwarerequest' element={<AnnualSoftwareRequestHome />} />
      <Route path='/annualsoftwarerequest/create' element={<CreateAnnualSoftwareRequest />} />
      <Route path='/annualsoftwarerequest/details/:id' element={<ShowAnnualSoftwareRequest />} />
      <Route path='/annualsoftwarerequest/edit/:id' element={<EditAnnualSoftwareRequest />} />
      <Route path='/annualsoftwarerequest/delete/:id' element={<DeleteAnnualSoftwareRequest />} />

      <Route path='/peopleneedingnewhardware' element={<PeopleNeedingNewHardwareHome />} />
      <Route path='/peopleneedingnewhardware/create' element={<CreatePeopleNeedingNewHardware />} />
      <Route path='/peopleneedingnewhardware/details/:id' element={<ShowPeopleNeedingNewHardware />} />
      <Route path='/peopleneedingnewhardware/edit/:id' element={<EditPeopleNeedingNewHardware />} />
      <Route path='/peopleneedingnewhardware/delete/:id' element={<DeletePeopleNeedingNewHardware />} />


      <Route path='/phdlaptopallocation' element={<PhdLaptopAllocationsHome />} />
      <Route path='/phdlaptopallocation/create' element={<CreatePhdLaptopAllocation />} />
      <Route path='/phdlaptopallocation/details/:id' element={<ShowPhdLaptopAllocation />} />
      <Route path='/phdlaptopallocation/edit/:id' element={<EditPhdLaptopAllocation />} />
      <Route path='/phdlaptopallocation/delete/:id' element={<DeletePhdLaptopAllocation />} />

      <Route path='/softwarelicences' element={<SoftwareLicencesHome />} />
      <Route path='/softwarelicences/create' element={<CreateSoftwareLicences />} />
      <Route path='/softwarelicences/details/:id' element={<ShowSoftwareLicences />} />
      <Route path='/softwarelicences/edit/:id' element={<EditSoftwareLicences />} />
      <Route path='/softwarelicences/delete/:id' element={<DeleteSoftwareLicences />} />

      <Route path='/softwaremodulemapping' element={<SoftwareModuleMappingHome />} />
      <Route path='/softwaremodulemapping/create' element={<CreateSoftwareModuleMapping />} />
      <Route path='/softwaremodulemapping/details/:id' element={<ShowSoftwareModuleMapping />} />
      <Route path='/softwaremodulemapping/edit/:id' element={<EditSoftwareModuleMapping />} />
      <Route path='/softwaremodulemapping/delete/:id' element={<DeleteSoftwareModuleMapping />} />

      <Route path='/unallocatedequipment' element={<UnallocatedEquipmentHome />} />
      <Route path='/unallocatedequipment/create' element={<CreateUnallocatedEquipment />} />
      <Route path='/unallocatedequipment/details/:id' element={<ShowUnallocatedEquipment />} />
      <Route path='/unallocatedequipment/edit/:id' element={<EditUnallocatedEquipment />} />
      <Route path='/unallocatedequipment/delete/:id' element={<DeleteUnallocatedEquipment />} />
    </Routes>
  )
}



export default App