import mongoose from "mongoose"

const adHocEquipmentSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        serialNo: {
            type: String,
            required: true,
        },
        staffContact: {
            type: String,
            required: true,
        }
    },
    {
        timestamps: true
    }
);

export const AdHocEquipment = mongoose.model("AdHocEquipment", adHocEquipmentSchema)