import mongoose from "mongoose"

const phdLaptopAllocationSchema = mongoose.Schema(
    {
        model: {
            type: String,
            required: true,
        },
        student: {
            type: String,
            required: true,
        },
        supervisor: {
            type: String,
            required: false,
        },
        macAddress: {
            type: String,
            required: false,
        },
        serialNo: {
            type: String,
            required: true,
        },
        purchaseOrder: {
            type: String,
            required: false,
        },
        monthAllocated: {
            type: String,
            required: false,
        },
        monthReturned: {
            type: String,
            required: false,
        },
    },
    {
        timestamps: true
    }
)

export const PhdLaptopAllocation = mongoose.model("PhdLaptopAllocation", phdLaptopAllocationSchema)