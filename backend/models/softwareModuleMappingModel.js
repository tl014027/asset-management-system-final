import mongoose from "mongoose"

const softwareModuleMappingSchema = mongoose.Schema(
    {
        moduleCode: {
            type: String,
            required: true,
        },
        title: {
            type: String,
            required: true,
        },
        softwareUsed: {
            type: String,
            reqiured: false,
        },
        softwareTaught: {
            type: String,
            required: false,
        },
        staffContact: {
            type: String,
            required: false,
        },
        lastVerified: {
            type: String,
            required: false,
        },
    },
    {
        timestamps: true
    }
);

export const SoftwareModuleMapping = mongoose.model("SoftwareModuleMapping", softwareModuleMappingSchema)