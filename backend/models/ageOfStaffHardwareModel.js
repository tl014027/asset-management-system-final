import mongoose from "mongoose"

const ageOfStaffHardwareSchema = mongoose.Schema(
    {
        hardwareType: {
            type: String,
            required: false,
        },
        hardwareModel: {
            type: String,
            required: true,
        },
        hardwareProcessor: {
            type: String,
            required: false
        },
        hardwareLocation: {
            type: String,
            required: false,
        },
        inuse: {
            type: String,
            required: true,
        },
        dispose: {
            type: String,
            required: false,
        },
        age: {
            type: String,
            required: true,
        }
    },
    {
        timestamps: true
    }
);

export const AgeOfStaffHardware = mongoose.model("AgeOfStaffHardware", ageOfStaffHardwareSchema)