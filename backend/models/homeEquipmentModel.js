import mongoose from "mongoose"

const homeEquipmentSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        equipment: {
            type: String,
            required: true,
        },
        serialNo: {
            type: String,
            required: false,
        },
        status: {
            type: String,
            required: false,
        },
    },
    {
        timestamps: true
    }
);

export const HomeEquipment = mongoose.model("HomeEquipment", homeEquipmentSchema)