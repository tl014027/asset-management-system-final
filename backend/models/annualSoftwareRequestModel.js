import mongoose from "mongoose"

const softwareSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        // TODO: This needs to deal with Latest as well. input 0.0.0 for latest version..?
        version: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: false,
        },
        configuration: {
            type: String,
            required: false,
        },
        operatingSystem: {
            type: String,
            required: true,
        },
        additionalHardware: {
            type: String,
            required: false,
        },
        contact: {
            type: String,
            required: false,
        },
        testSteps: {
            type: String,
            required: false,
        },
        // TODO: Change type to date
        dateRequired: {
            type: String,
            required: true,
        },
        offCampus: {
            type: String,
            required: true,
        },
        // TODO: Create list that accepts certain inputs. G56 etc.
        whichLocations: {
            type: String,
            required: true,
        },
        accessLimit: {
            type: String,
            required: true,
        },
        // TODO: Appear to be 3 values for this. How to store efficiently
        licenceHeld: {
            type: String,
            required: true,
        },
        productManufacturer: {
            type: String,
            required: true,
        },
        other: {
            type: String,
            required: true,
        },
        // TODO: Custom ticket model C-2304-323
        changeTicket: {
            type: String,
            required: true,
        },
        // TODO: Change type to date
        dateRequested: {
            type: String,
            required: true,
        },
        // TODO: Again, should accept in progress or submitted
        status: {
            type: String,
            required: true,
        },

    },
    {
        timestamps: true
    }
)

export const Software = mongoose.model("Software", softwareSchema)