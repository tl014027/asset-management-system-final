import mongoose from "mongoose"

const computerSchema = mongoose.Schema(
    {
        type: {
            type: String,
            required: true,
        },
        model: {
            type: String,
            required: true,
        },
        characteristics: {
            type: String,
            required: true,
        },
        responsibleStaff: {
            type: String,
            required: false,
        },
        location: {
            type: String,
            required: true,
        },
        machineStatus: {
            type: String,
            required: true,
        },
        macAddress: {
            type: String,
            required: true,
        },
        assetTag: {
            type: String,
            required: true,
        },
        invNo: {
            type: String,
            required: true,
        },
        graphicsCard: {
            type: String,
            required: true,
        },
        comments: {
            type: String,
            required: false,
        },
        serialNo: {
            type: String,
            required: true,
        },
        alternateUsername: {
            type: String,
            required: false,
        },
        warrantyStartDate: {
            type: String,
            required: true,
        },
        orderNo: {
            type: String,
            required: true,
        },
    },
    {
        timestamps: true
    }
);

export const Computer = mongoose.model("Computer", computerSchema)