import mongoose from "mongoose"

const peopleNeedingNewHardwareSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        inUse: {
            type: String,
            required: false
        }
    },
    {
        timestamps: true
    }
);

export const PeopleNeedingNewHardware = mongoose.model("PeopleNeedingNewHardware", peopleNeedingNewHardwareSchema)