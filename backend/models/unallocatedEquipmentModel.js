import mongoose from "mongoose"

const unallocatedEquipmentSchema = mongoose.Schema(
    {
        macAddress: {
            type: String,
            required: false,
        },
        pcNameLabel: {
            type: String,
            required: true,
        },
        inventoryNo: {
            type: String,
            required: false,
        },
        graphicsCard: {
            type: String,
            required: true,
        },
        comments: {
            type: String,
            required: false,
        },
        serialNo: {
            type: String,
            required: true,
        },
        alternateUsername: {
            type: String,
            required: false,
        },
        startDate: {
            type: String,
            required: true,
        },
        orderNo: {
            type: String,
            required: false
        },
    },
    {
        timestamps: true
    }
);

export const UnallocatedEquipment = mongoose.model("UnallocatedEquipment", unallocatedEquipmentSchema)