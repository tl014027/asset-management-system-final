import mongoose from "mongoose"

const softwareLicencesSchema = mongoose.Schema(
    {
        program: {
            type: String,
            required: true,
        },
        licenceNo: {
            type: String,
            required: false,
        },
        expiry: {
            type: Date,
            required: false,
        },
        numberOfLicences: {
            type: String,
            required: true,
        },
        memberOfStaff: {
            type: String,
            required: true,
        }
    },
    {
        timestamps: true
    }
);

export const SoftwareLicences = mongoose.model("SoftwareLicences", softwareLicencesSchema)