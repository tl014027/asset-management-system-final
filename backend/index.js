import express from "express";
import { PORT, MONGO_DB_URL } from "./config.js";
import mongoose from 'mongoose'
import computerRoute from './routes/computerRoute.js'
import annualSoftwareRequestRoute from './routes/annualSoftwareRequestRoute.js'
import homeEquipmentRoute from './routes/homeEquipmentRoute.js'
import softwareModuleMapping from './routes/softwareModuleMappingRoute.js'
import adHocEquipment from "./routes/adHocEquipmentRoute.js";
import phdLaptopAllocation from './routes/phdLaptopAllocationRoute.js';
import softwareLicences from "./routes/softwareLicencesRoute.js";
import unallocatedEquipment from "./routes/unallocatedEquipmentRoute.js"
import peopleNeedingNewHardware from "./routes/peopleNeedingNewHardwareRoute.js"
import ageOfStaffHardware from "./routes/ageOfStaffHardwareRoute.js"
import cors from 'cors';

const app = express();

app.use(express.json());

app.use(cors());

app.get('/', (request, response) => {
    console.log(request)
    return response.status(234).send('Route is functional')
});

app.use('/computer', computerRoute);
app.use('/annualsoftwarerequest', annualSoftwareRequestRoute);
app.use('/homeequipment', homeEquipmentRoute);
app.use('/softwaremodulemapping', softwareModuleMapping);
app.use('/adhocequipment', adHocEquipment);
app.use('/phdlaptopallocation', phdLaptopAllocation);
app.use('/softwareLicences', softwareLicences);
app.use('/unallocatedEquipment', unallocatedEquipment);
app.use('/peopleneedingnewhardware', peopleNeedingNewHardware);
app.use('/ageofstaffhardware', ageOfStaffHardware);

mongoose
    .connect(MONGO_DB_URL)
    .then(() => {
        console.log('App connected to the database');
        app.listen(PORT, () => {
            console.log('Server is running on port: ' + PORT);
        })
    })
    .catch((error) => {
        console.log(error);
    });