import express from "express";
import { AdHocEquipment } from "../models/adHocEquipmentModel.js";

const router = express.Router();

function checkHeaders(request) {
    return (request.body.name &&
        request.body.serialNo &&
        request.body.staffContact
    );
}

// Route to save a new adHocEquipment
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round and yet still working..?
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newAdHocEquipment = {
            name: request.body.name,
            serialNo: request.body.serialNo,
            staffContact: request.body.staffContact
        };
        const adHocEquipment = await AdHocEquipment.create(newAdHocEquipment);
        return response.status(201).send(adHocEquipment);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get adHocEquipments from database
router.get("/", async (request, response) => {
    try {
        const adHocEquipments = await AdHocEquipment.find({});
        return response.status(200).json({
            count: adHocEquipments.length,
            data: adHocEquipments
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get a single adHocEquipment from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const adHocEquipment = await AdHocEquipment.findById(id);

        return response.status(200).json(adHocEquipment)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating adHocEquipment
router.put("/:id", async (request, response) => {
    try {
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await AdHocEquipment.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "AdHocEquipment not found" })
        }
        return response.status(200).send({ message: "AdHocEquipment updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting adHocEquipment
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await AdHocEquipment.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "AdHocEquipment not found" })
        }

        return response.status(200).json({ message: "AdHocEquipment deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;