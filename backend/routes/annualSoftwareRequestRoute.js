import express from "express";
import { Software } from "../models/annualSoftwareRequestModel.js"

const router = express.Router();

function checkHeaders(request) {
    return (request.body.name &&
        request.body.version &&
        request.body.description &&
        request.body.configuration &&
        request.body.operatingSystem &&
        request.body.additionalHardware &&
        request.body.contact &&
        request.body.testSteps &&
        request.body.dateRequired &&
        request.body.offCampus &&
        request.body.whichLocations &&
        request.body.accessLimit &&
        request.body.licenceHeld &&
        request.body.productManufacturer &&
        request.body.other &&
        request.body.changeTicket &&
        request.body.dateRequested &&
        request.body.status
    );
}

// Route to save new software
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round. Temp fix is !.
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newSoftware = {
            name: request.body.name,
            version: request.body.version,
            description: request.body.description,
            configuration: request.body.configuration,
            operatingSystem: request.body.operatingSystem,
            additionalHardware: request.body.additionalHardware,
            contact: request.body.contact,
            testSteps: request.body.testSteps,
            dateRequired: request.body.dateRequired,
            offCampus: request.body.offCampus,
            whichLocations: request.body.whichLocations,
            accessLimit: request.body.accessLimit,
            licenceHeld: request.body.licenceHeld,
            productManufacturer: request.body.productManufacturer,
            other: request.body.other,
            changeTicket: request.body.changeTicket,
            dateRequested: request.body.dateRequested,
            status: request.body.status,
        };
        const software = await Software.create(newSoftware);
        return response.status(201).send(software);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get software from database
router.get("/", async (request, response) => {
    try {
        const software = await Software.find({});
        return response.status(200).json({
            count: software.length,
            data: software
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get single software from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const software = await Software.findById(id);

        return response.status(200).json(software)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating software
router.put("/:id", async (request, response) => {
    try {
        if (!checkHeaders(request)) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await Software.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "Software not found" })
        }
        return response.status(200).send({ message: "Software updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting software
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await Software.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "Software not found" })
        }

        return response.status(200).json({ message: "Software deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;