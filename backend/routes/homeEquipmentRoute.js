import express from "express";
import { HomeEquipment } from "../models/homeEquipmentModel.js"

const router = express.Router();

function checkHeaders(request) {
    return (!request.body.name &&
        request.body.equipment &&
        request.body.serialNo &&
        request.body.status
    );
}

// Route to save a new home equipment
router.post("/", async (request, response) => {
    try {
        if (checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newHomeEquipment = {
            name: request.body.name,
            equipment: request.body.equipment,
            serialNo: request.body.serialNo,
            status: request.body.status,
        };
        const homeEquipment = await HomeEquipment.create(newHomeEquipment);
        return response.status(201).send(homeEquipment);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get home equipment from database
router.get("/", async (request, response) => {
    try {
        const homeEquipmentList = await HomeEquipment.find({});
        return response.status(200).json({
            count: homeEquipmentList.length,
            data: homeEquipmentList
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get a single HomeEquipment from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const homeEquipment = await HomeEquipment.findById(id);

        return response.status(200).json(homeEquipment)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating home equipment
router.put("/:id", async (request, response) => {
    try {
        if (checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await HomeEquipment.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "Equipment not found" })
        }
        return response.status(200).send({ message: "Equipment updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting equipment
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await HomeEquipment.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "Equipment not found" })
        }

        return response.status(200).json({ message: "Equipment deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;