import express from "express";
import { PeopleNeedingNewHardware } from "../models/peopleNeedingNewHardwareModel.js";

const router = express.Router();

function checkHeaders(request) {
    return (request.body.name &&
        request.body.inuse
    );
}

// Route to save a new peopleNeedingNewHardware
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round and yet still working..?
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newPeopleNeedingNewHardware = {
            name: request.body.name,
            inuse: request.body.inuse
        };
        const peopleNeedingNewHardware = await PeopleNeedingNewHardware.create(newPeopleNeedingNewHardware);
        return response.status(201).send(peopleNeedingNewHardware);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get peopleNeedingNewHardwares from database
router.get("/", async (request, response) => {
    try {
        const peopleNeedingNewHardwares = await PeopleNeedingNewHardware.find({});
        return response.status(200).json({
            count: peopleNeedingNewHardwares.length,
            data: peopleNeedingNewHardwares
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get a single peopleNeedingNewHardware from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const peopleNeedingNewHardware = await PeopleNeedingNewHardware.findById(id);

        return response.status(200).json(peopleNeedingNewHardware)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating peopleNeedingNewHardware
router.put("/:id", async (request, response) => {
    try {
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await PeopleNeedingNewHardware.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "PeopleNeedingNewHardware not found" })
        }
        return response.status(200).send({ message: "PeopleNeedingNewHardware updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting peopleNeedingNewHardware
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await PeopleNeedingNewHardware.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "PeopleNeedingNewHardware not found" })
        }

        return response.status(200).json({ message: "PeopleNeedingNewHardware deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;