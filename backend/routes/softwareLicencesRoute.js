import express from "express";
import { SoftwareLicences } from "../models/softwareLicencesModel.js";

const router = express.Router();

function checkHeaders(request) {
    return (request.body.program &&
        request.body.numberOfLicences &&
        request.body.memberOfStaff
    );
}

// Route to save a new softwareLicences
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round and yet still working..?
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newSoftwareLicences = {
            program: request.body.program,
            licenceNo: request.body.licenceNo,
            expiry: request.body.expiry,
            numberOfLicences: request.body.numberOfLicences,
            memberOfStaff: request.body.memberOfStaff
        };
        const softwareLicences = await SoftwareLicences.create(newSoftwareLicences);
        return response.status(201).send(softwareLicences);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get softwareLicences from database
router.get("/", async (request, response) => {
    try {
        const softwareLicences = await SoftwareLicences.find({});
        return response.status(200).json({
            count: softwareLicences.length,
            data: softwareLicences
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get a single softwareLicences from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const softwareLicences = await SoftwareLicences.findById(id);

        return response.status(200).json(softwareLicences)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating softwareLicences
router.put("/:id", async (request, response) => {
    try {
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await SoftwareLicences.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "SoftwareLicences not found" })
        }
        return response.status(200).send({ message: "SoftwareLicences updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting softwareLicences
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await SoftwareLicences.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "SoftwareLicences not found" })
        }

        return response.status(200).json({ message: "SoftwareLicences deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;