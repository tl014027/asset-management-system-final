import express from "express";
import { SoftwareModuleMapping } from "../models/softwareModuleMappingModel.js"

const router = express.Router();

function checkHeaders(request) {
    return (!
        request.body.moduleCode &&
        request.body.title &&
        request.body.softwareUsed &&
        request.body.softwareTaught &&
        request.body.staffContact &&
        request.body.lastVerified
    );
}

// Route to save new Software Module Mapping
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round. Temp fix is !.
        if (checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newSoftwareModuleMapping = {
            moduleCode: request.body.moduleCode,
            title: request.body.title,
            softwareUsed: request.body.softwareUsed,
            softwareTaught: request.body.softwareTaught,
            staffContact: request.body.staffContact,
            lastVerified: request.body.lastVerified
        };
        const softwareModuleMapping = await SoftwareModuleMapping.create(newSoftwareModuleMapping);
        return response.status(201).send(softwareModuleMapping);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get software module mapping from database
router.get("/", async (request, response) => {
    try {
        const softwareModuleMapping = await SoftwareModuleMapping.find({});
        return response.status(200).json({
            count: softwareModuleMapping.length,
            data: softwareModuleMapping
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get single software module mapping from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const softwareModuleMapping = await SoftwareModuleMapping.findById(id);

        return response.status(200).json(softwareModuleMapping)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating software module mapping
router.put("/:id", async (request, response) => {
    try {
        if (checkHeaders(request)) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await SoftwareModuleMapping.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "Software Module Mapping not found" })
        }
        return response.status(200).send({ message: "Software Module Mapping updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting software module mapping
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await SoftwareModuleMapping.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "Software module mapping not found" })
        }

        return response.status(200).json({ message: "Software module mapping deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;