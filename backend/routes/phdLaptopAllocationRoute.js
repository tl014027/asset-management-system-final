import express from "express";
import { PhdLaptopAllocation } from "../models/phdLaptopAllocationModel.js"

const router = express.Router();

function checkHeaders(request) {
    return (
        request.body.model &&
        request.body.student &&
        request.body.supervisor &&
        request.body.macAddress &&
        request.body.serialNo &&
        request.body.purchaseOrder &&
        request.hody.monthAllocated &&
        request.body.monthReturned
    );
}

// Route to save new phd laptop allocation
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round. Temp fix is !.
        if (checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newPhdLaptopAllocation = {
            model: request.body.model,
            student: request.body.student,
            supervisor: request.body.supervisor,
            macAddress: request.body.macAddress,
            serialNo: request.body.serialNo,
            purchaseOrder: request.body.purchaseOrder,
            monthAllocated: request.body.monthAllocated,
            monthReturned: request.body.monthReturned
        };
        const phdLaptopAllocation = await PhdLaptopAllocation.create(newPhdLaptopAllocation);
        return response.status(201).send(phdLaptopAllocation);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get phd laptop allocation from database
router.get("/", async (request, response) => {
    try {
        const phdLaptopAllocation = await PhdLaptopAllocation.find({});
        return response.status(200).json({
            count: phdLaptopAllocation.length,
            data: phdLaptopAllocation
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get single phd laptop allocation from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const phdLaptopAllocation = await PhdLaptopAllocation.findById(id);

        return response.status(200).json(phdLaptopAllocation)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating phd laptop allocation
router.put("/:id", async (request, response) => {
    try {
        if (checkHeaders(request)) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await PhdLaptopAllocation.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "PHD Laptop Allocation not found" })
        }
        return response.status(200).send({ message: "PHD Laptop Allocation updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting software
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await PhdLaptopAllocation.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "PHD Laptop Allocation not found" })
        }

        return response.status(200).json({ message: "PHD Laptop Allocation deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;