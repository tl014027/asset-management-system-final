import express from "express";
import { UnallocatedEquipment } from "../models/unallocatedEquipmentModel.js";

const router = express.Router();

function checkHeaders(request) {
    return (
        request.body.macAddress &&
        request.body.pcNameLabel &&
        request.body.inventoryNo &&
        request.body.graphicsCard &&
        request.body.comments &&
        request.body.serialNo &&
        request.body.alternateUsername &&
        request.body.startDate &&
        request.body.orderNo
    );
}

// Route to save a new unallocatedEquipment
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round and yet still working..?
        if (checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newUnallocatedEquipment = {
            macAddress: request.body.macAddress,
            pcNameLabel: request.body.pcNameLabel,
            inventoryNo: request.body.inventoryNo,
            graphicsCard: request.body.graphicsCard,
            comments: request.body.comments,
            serialNo: request.body.serialNo,
            alternateUsername: request.body.alternateUsername,
            startDate: request.body.startDate,
            orderNo: request.body.orderNo,
        };
        const unallocatedEquipment = await UnallocatedEquipment.create(newUnallocatedEquipment);
        return response.status(201).send(unallocatedEquipment);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get unallocatedEquipments from database
router.get("/", async (request, response) => {
    try {
        const unallocatedEquipments = await UnallocatedEquipment.find({});
        return response.status(200).json({
            count: unallocatedEquipments.length,
            data: unallocatedEquipments
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get a single unallocatedEquipment from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const unallocatedEquipment = await UnallocatedEquipment.findById(id);

        return response.status(200).json(unallocatedEquipment)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating unallocatedEquipment
router.put("/:id", async (request, response) => {
    try {
        if (checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await UnallocatedEquipment.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "Unallocated Equipment not found" })
        }
        return response.status(200).send({ message: "Unallocated Equipment updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting unallocatedEquipment
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await UnallocatedEquipment.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "Unallocated Equipment not found" })
        }

        return response.status(200).json({ message: "Unallocated Equipment deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;