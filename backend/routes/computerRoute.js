import express from "express";
import { Computer } from "../models/computerModel.js"

const router = express.Router();

function checkHeaders(request) {
    return (request.body.type &&
        request.body.model &&
        request.body.characteristics &&
        request.body.responsibleStaff &&
        request.body.location &&
        request.body.machineStatus &&
        request.body.macAddress &&
        request.body.assetTag &&
        request.body.invNo &&
        request.body.graphicsCard &&
        request.body.comments &&
        request.body.serialNo &&
        request.body.alternateUsername &&
        request.body.warrantyStartDate &&
        request.body.order
    );
}

// Route to save a new computer
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round and yet still working..?
        if (checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newComputer = {
            type: request.body.type,
            model: request.body.model,
            characteristics: request.body.characteristics,
            responsibleStaff: request.body.responsibleStaff,
            location: request.body.location,
            machineStatus: request.body.machineStatus,
            macAddress: request.body.macAddress,
            assetTag: request.body.assetTag,
            invNo: request.body.invNo,
            graphicsCard: request.body.graphicsCard,
            comments: request.body.comments,
            serialNo: request.body.serialNo,
            alternateUsername: request.body.alternateUsername,
            warrantyStartDate: request.body.warrantyStartDate,
            orderNo: request.body.orderNo,
        };
        const computer = await Computer.create(newComputer);
        return response.status(201).send(computer);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get computers from database
router.get("/", async (request, response) => {
    try {
        const computers = await Computer.find({});
        return response.status(200).json({
            count: computers.length,
            data: computers
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get a single computer from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const computer = await Computer.findById(id);

        return response.status(200).json(computer)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating computer
router.put("/:id", async (request, response) => {
    try {
        if (checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await Computer.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "Computer not found" })
        }
        return response.status(200).send({ message: "Computer updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting computer
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await Computer.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "Computer not found" })
        }

        return response.status(200).json({ message: "Computer deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;