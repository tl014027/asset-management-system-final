import express from "express";
import { AgeOfStaffHardware } from "../models/ageOfStaffHardwareModel.js";

const router = express.Router();

function checkHeaders(request) {
    return (
        request.body.hardwareType &&
        request.body.hardwareModel &&
        request.body.hardwareProcessor &&
        request.body.hardwareLocation &&
        request.body.inuse &&
        request.body.dispose &&
        request.body.age
    );
}

// Route to save a new ageOfStaffHardware
router.post("/", async (request, response) => {
    try {
        // FIXME: If checkHeaders fails it should return an error, this is currently the wrong way round and yet still working..?
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields.",
            })
        }
        const newAgeOfStaffHardware = {
            hardwareType: request.body.hardwareType,
            hardwareModel: request.body.hardwareModel,
            hardwareProcessor: request.body.hardwareProcessor,
            hardwareLocation: request.body.hardwareLocation,
            inuse: request.body.inuse,
            dispose: request.body.dispose,
            age: request.body.age,
        };
        const ageOfStaffHardware = await AgeOfStaffHardware.create(newAgeOfStaffHardware);
        return response.status(201).send(ageOfStaffHardware);
    } catch (error) {
        console.log(error.message)
        response.status(500).send({ message: error.message })
    }
})

// Route to get ageOfStaffHardwares from database
router.get("/", async (request, response) => {
    try {
        const ageOfStaffHardwares = await AgeOfStaffHardware.find({});
        return response.status(200).json({
            count: ageOfStaffHardwares.length,
            data: ageOfStaffHardwares
        })
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route to get a single ageOfStaffHardware from database by ID
router.get("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const ageOfStaffHardware = await AgeOfStaffHardware.findById(id);

        return response.status(200).json(ageOfStaffHardware)
    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for updating ageOfStaffHardware
router.put("/:id", async (request, response) => {
    try {
        if (!checkHeaders(request)
        ) {
            return response.status(400).send({
                message: "Send all required fields",
            })
        }

        const { id } = request.params;
        const result = await AgeOfStaffHardware.findByIdAndUpdate(id, request.body);
        if (!result) {
            return response.status(404).json({ message: "Age of staff hardware object not found" })
        }
        return response.status(200).send({ message: "Age of staff hardware object updated successfully" });

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

// Route for deleting ageOfStaffHardware
router.delete("/:id", async (request, response) => {
    try {
        const { id } = request.params;

        const result = await AgeOfStaffHardware.findByIdAndDelete(id);

        if (!result) {
            return response.status(404).json({ message: "Age of staff hardware object not found" })
        }

        return response.status(200).json({ message: "Age of staff hardware object deleted successfully" })

    } catch (error) {
        console.log(error.message);
        response.status(500).send({ message: error.message })
    }
})

export default router;